﻿using UnityEditor;
using System;
using System.IO;

public class WindowsStandalone
{
    static void Build()
    {
        string[] scenes = { "Assets/Scenes/Apartment.unity" };

        string path = Directory.GetCurrentDirectory();
        string target = path + "/Builds";

        if (!Directory.Exists(target))
        {
            Directory.CreateDirectory(target);
        }

        BuildPipeline.BuildPlayer(scenes, target + "/build.exe", BuildTarget.StandaloneWindows, BuildOptions.None);
    }
}
